<?php
/**
 * @file
 * LinkResolverCrud class file
 *
 */
 
/**
 * Handle C.R.U.D for resolver data
 * 
 * @package LinkResolver
 * @author Chad Fennell
 **/
class LinkResolverCrud extends LinkResolver {
  
  public function insert($institution_name, $resolver_url, $override_resolver_url = NULL, $registry_id = NULL, $prid = NULL) {
    $rid = db_next_id('{resolvers}_rid');
    db_query('INSERT INTO {resolvers} (rid, prid, institution_name, resolver_url, override_resolver_url, registry_id, updated) VALUES (%d, %d, \'%s\', \'%s\', %d, %d, %d)', 
      $rid, $prid, $institution_name, $resolver_url, $override_resolver_url, $registry_id, time());
    return $rid;
  }
  
  public function update($institution_name, $resolver_url, $override_resolver_url = NULL, $prid = NULL) {
    if (self::get_rid()) {
      db_query("UPDATE {resolvers} SET prid=%d, institution_name='%s', resolver_url='%s', override_resolver_url=%d, updated=%d WHERE rid=%d", 
        $prid, $institution_name, $resolver_url, $override_resolver_url, time(), self::get_rid());        
    }
  }
  
  public function delete() {
    if (self::get_rid()) {
      db_query("DELETE FROM {resolvers} WHERE rid=%d", self::get_rid());
    }
  }
  
  public function assign_user_to_rid($rid, $uid) {
    if ($uid >= 1 && $rid) { //sanity check
      db_query("DELETE FROM {resolvers_uid_rid} WHERE uid=%d",$uid);    
      db_query('INSERT INTO {resolvers_uid_rid} (rid, uid) VALUES (%d, %d)', $rid, $uid);
    }
  }
  
  public function reassign_users_to_rid($new_rid) {
    db_query("UPDATE {resolvers_uid_rid} SET rid=%d WHERE rid=%d", $new_rid, self::get_rid());
  }
  
  public function delete_prefs_for_rid() {
    db_query("DELETE FROM {resolvers_uid_rid} WHERE rid=%d", self::get_rid());
  }
  
  public function delete_prefs_for_uid($uid) {
    db_query("DELETE FROM {resolvers_uid_rid} WHERE uid=%d", $uid);
  }
}