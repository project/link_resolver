<?php


class LinkResolverMessages extends LinkResolver{
  private $_message;
  private $_institution;
  private $preference_path;
  
  function LinkResolverMessages($institution) {
    global $user;
    
    $this->preferences_path = sprintf('user/%d/edit/%s', $user->uid, variable_get('resolver_preferences_path', 'library_link'));
    
    $this->_institution = $institution;
    
    if ($user->uid >= 1) {
      self::authenticated();
    }
    else{
      self::anonymous();
    }
  }
    
  public function get_message() {
    return $this->_message;
  }

  private function authenticated() {
    if ($this->_institution ) {
      $message = variable_get('block_resolver_authenticated_success', '');
      $preferences_link = l('Library Link Preference', $this->preferences_path);      
    }
    else {
      $message = variable_get('block_resolver_authenticated_failed', '');
      $preferences_link = l('Set your Library Link Preference ', $this->preferences_path);            
    }
    
    self::set_message($message, $preferences_link);
  }
  
  private function anonymous() {
    if ($this->_institution) {
      $message = variable_get('block_resolver_anonymous_success', '');
      $preferences_link = l('Log In/Register', 'user', NULL, 'destination='. $this->preferences_path);      
    }
    else {
      $message = variable_get('block_resolver_anonymous_failed', '');
      $preferences_link = l('Log In/Register', 'user', NULL, 'destination='. $this->preferences_path);      
    }
    
    self::set_message($message, $preferences_link);
  }
  
  private function set_message($message, $preferences_link) {
    $this->_message = t($message,
      array(
        '!link_preference_help'   => l('Library Link Preference', 'library_link_preference/help'),
        '!setting'                => sprintf('<strong>%s</strong>', $this->_institution),
        '!preferences_link'       => $preferences_link,      
      )
    );
  }
}
  
