<?php
/**
 * @file
 * LinkResolverInit class file
 *
 */

/**
 * LinkResolverInit
 *
 * Initializes LinkResolver Data vai OCLC Gateway, User Preferences or Cookie 
 * Lookup.  Most of the work of this module happens here
 * 
 * @package LinkResolver
 * @author Chad Fennell
 **/
 
class LinkResolverInit extends LinkResolver {
   private $_ip;

  /*
  * setter for ip - default is $_SERVER['REMOTE_ADDR'];
  */
  public function set_ip($ip) {
    $this->_ip = $ip;
  }
  
  public function get_ip() {
    return $this->_ip;
  }
  
  /*
  * set LinkResolver object data
  */
  public function initialize_resolver($ip = NULL) {
    $ip = ($ip) ? $ip : $_SERVER['REMOTE_ADDR'];
    self::set_ip($ip);
    $uid = self::get_uid();
    if ($uid == 0) {
      self::anonymous();
    }
    else {
      self::authenticated();
    }
  }

  /*
  * Set resolver data for a logged-in user
  */  
  private function authenticated() {  
    //If user has set prefs, use them and bypass everything else
    if (self::init_by_user_preferences() != TRUE) {
      
      //init by cookie data or by OLCL IP lookup
      self::init_by_external_data();
    
      //If we found or added a local resolver, set the user's preference
      if (self::get_rid()) {
        self::set_preference();
      }
    }
  }
  
  /*
  * set resolver data for non-logged-in user
  */  
  private function anonymous() {
    //init by cookie data or by OLCL IP lookup
    self::init_by_external_data();
  }
  
  /*
  * Set the resolver data via (a) a cookie or (b) a lookup on the WorldCat 
  * registry /if no cookie exists/. Reset the resolver data to the parent 
  * resolver data if a parent exists for a given resolver.
  * 
  * NOTE: Cookie checks prevent search from bogging down due too many OCLC IP lookups.
  * b/c cookies are client-side, users can succeed in automatic preference detection/setting
  * at work and have this carry over to the home environment where an IP lookup would otherwise
  * fail.
  * 
  * @return void
  */
  private function init_by_external_data() {
    //If no cookie has been set, try looking up by IP
    if (self::init_by_cookie() === FALSE) {
      //set institution data if locatable in resolver gateway
      self::init_by_gateway_ip_institution_lookup();               
      //check to see if we know about this resolver locally
      //if not, add it and get a local rid
      if (self::init_by_local_registry_id() == FALSE) {
        //if we have some good resolver data, let's add this resolver to our local db
        if (self::get_resolver_url() && self::get_institution_name()) {
          self::add_resolver();
        }
      }
      else {
        //If we found a local resolver w/this registry id, check for a parent id
        //if we have one, stop the presses and use that resolver's data
        if (self::get_prid() > 0) {
          self::reset_to_parent_rid(self::get_prid());
        }        
      }
      //sets resolver cookie data; sets "no-resolver" on failed lookup
      self::set_cookie(); 
    }  
  }
  
  /*
  * check to see if we have resover data in a cookie
  * this means we don't have to hit OCLC on every page load
  */
  public function init_by_cookie() {
    if (strpos($_COOKIE["resolver_data"], "|") != FALSE) {
      $data = explode('|', $_COOKIE["resolver_data"]);
      //if a parent ID exists, reset this cookie
      $prid = parent::get_prid_for_rid($data[2]);
      if ($prid) {
        //resets class property data to parent rid    
        self::reset_to_parent_rid($prid);
        self::set_cookie();
      }
      else {
        $institution_name = check_plain($data[0]);
        $resolver_url = check_plain($data[1]);
        $rid = (int) check_plain($data[2]);
        $registry_id = (int) check_plain($data[3]);
        self::set_institution_name($institution_name);
        self::set_resolver_url($resolver_url);
        self::set_rid($rid);
        self::set_registry_id($registry_id);
        return TRUE;
      }
    }
    elseif($_COOKIE["resolver_data"]) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /*
  * Set resolver data in a cookie
  * We only set resolvers for institutions in the registry
  * Hence, the registry_id here
  */    
  private function set_cookie() {
    //expires in one year
    if (self::get_resolver_url()) {
      setcookie("resolver_data", self::get_institution_name() .'|'. self::get_resolver_url() .'|'. self::get_rid() .'|'. self::get_registry_id(), time() + 31556926);
    }
    else {
      //only set this for 24 hours - allows for possibility to find a resolver
      //for folks w/laptops as they move between home and campus
     setcookie("resolver_data", 'no-resolver', time() + 86400);
    }
  }
  
  /*
  * Check to see if there is a parent ID for a given resolver, set the resolver
  * data to this parent resolver if so. Init by parent assumes a rid has already
  * been set.
  * 
  * @return bool 
  */  
  private function reset_to_parent_rid() {
    $result = db_query('SELECT * from {resolvers} WHERE rid=(SELECT prid FROM {resolvers} WHERE prid IS NOT NULL AND rid=%d)', self::get_rid());
    if ($result) {
      $resolver = db_fetch_object($result);
      self::set_rid($resolver->rid);
      self::set_registry_id($resolver->registry_id);      
      self::set_resolver_url($resolver->resolver_url);
      self::set_institution_name($resolver->institution_name);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  
  /*
  * Check to see if logged-in users have a preference, set resolver data if so
  */
  public function init_by_user_preferences() {
    $uid = self::get_uid();
    $resolver = parent::get_resolver_for_uid($uid);
    if ($resolver) {
      self::set_rid($resolver->rid);
      self::set_registry_id($resolver->registry_id);      
      self::set_resolver_url($resolver->resolver_url);
      self::set_institution_name($resolver->institution_name);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /*
  * Assign a resolver preference for a user
  */
  private function set_preference() {
    $uid = (int) self::get_uid();
    $rid = (int) self::get_rid();    
    //checking to see if this resolver exists in the db
    $resolver = parent::lookup_by_rid();
    if ($uid >= 1 && $rid && $resolver) {
      $resolver_crud = new LinkResolverCrud();
      $preferences_path = variable_get('resolver_preferences_path', '');
      $resolver_crud->assign_user_to_rid($rid, $uid);
         $message = t('Your !preference_help has been automatically been set based on your current physical location.  
          You may override this setting in your !preferences', 
          array(
            '!preference_help' => l('Library Link Preference', 'library_link_preference/help'),
            '!preferences' => l('Search Preferences', $preferences_path)
          )
      );
      drupal_set_message($message);
    }
  }
  
  /**
   * Request institutional data for a given IP from the OCLC Gateway
   */
  private function gateway_ip_institution_lookup(){
    if (self::get_ip()) {
      $url = sprintf('http://worldcatlibraries.org/registry/lookup?IP=%s', self::get_ip());
      if ($xml = @simplexml_load_file($url)) {      
        //grab all the institutions that match, try to get rid of the ILL resolvers
        foreach ($xml->resolverRegistryEntry as $entry) {
          //get the inst name from "enhancedContent" url because they tend to be better there...
          $enhanced = @simplexml_load_file('http://www.worldcat.org/webservices/registry/enhancedContent/Institutions/'. $entry->InstitutionID);
          $institution = $enhanced->nameLocation->institutionName;          
          if ($institution && $entry->resolver->baseURL && $entry->InstitutionID != 0) {  //yes, found one that had an id of zero (Iowa 128.255.8.83)
            $registry_id      = (int) $entry->InstitutionID;
            $inst             =       $institution;
            $url              =       parent::fix_resolver_urls($entry->resolver->baseURL);
            $institutions[]   =       $entry->institutionName;
            $resolvers_all[]  =       array('institution_name' => $inst, 'resolver_url' => $url, 'registry_id' => $registry_id);     
            if (!stripos($url, 'illiad')) {
              $resolvers_preferred[] = array('institution_name' => $inst, 'resolver_url' => $url, 'registry_id' => $registry_id);
            }
          }
        }
        //look for non-illiad "true" resolvers and use the first one
        if (count($resolvers_preferred) >= 1) {
          rsort($resolvers_preferred);
          $resolver = array_pop($resolvers_preferred);
        }
        else {
          rsort($resolvers_all);
          $resolver = array_pop($resolvers_all);
        }
            
      return $resolver;
      }
    }
    else {
      watchdog('OCLC Gateway', 'I Couldn\'t detect the User\'s IP Address <sigh>. - '. self::get_ip(), 1);
      
      return NULL;
    }
  }
  
  /**
   * Initialize resolver data based on the "best guess" (ie from the first response)
   */
  function init_by_gateway_ip_institution_lookup() {
    $resolver = self::gateway_ip_institution_lookup(); 
    if ($resolver != NULL) {
      self::set_institution_name($resolver['institution_name']);
      self::set_resolver_url($resolver['resolver_url']);
      self::set_registry_id($resolver['registry_id']);
    }
  }
  
  /*
  * Add a new resolver and set the rid
  */
  private function add_resolver() {
    $resolver_crud = new LinkResolverCrud();    
    $rid = $resolver_crud->insert(self::get_institution_name(), self::get_resolver_url(), 0, self::get_registry_id());
    self::set_rid($rid);    
    $message = sprintf('RID=%d, INSTITUTION=%s, RESOLVER_URL=%s, registry_id=%d %s', $rid, self::get_institution_name(), self::get_resolver_url(), self::get_registry_id(), $link);
    watchdog('Link Resolver', 'New Resolver: '. $message);    
    if (variable_get('resolver_notifications_address', '') != '') {
      $address = variable_get('resolver_notifications_address', '');
      $link = l('edit this resolver', 'admin/link_resolver/edit', NULL, '?rid='.$rid);
      drupal_mail('new-resolver-added', $address, 'New Resolver Added at '. $_SERVER['SERVER_ADDR'], $message, variable_get('site_mail', ''));
    }
  }
  
  /*
  * See if we know about this registry_id and update it if we do
  */
  private function init_by_local_registry_id() {
    if (self::get_registry_id()) {
      $result = db_query('SELECT * from {resolvers} where registry_id=%d', self::get_registry_id());
      $row = db_fetch_object($result);
    }
    if ($row) {  
      self::set_rid($row->rid);
      //we've just hit the gateway, so let's update our local resolver url
      //unless we have an overriden url
      if ($row->override_resolver_url == 0) {
        $resolver_crud = new LinkResolverCrud(); 
        $resolver_crud->set_rid($row->rid);
        $resolver_crud->update($row->institution_name, self::get_resolver_url());
      }
      else {
        //Override Resolver URL to locally stored url
        self::set_resolver_url($row->resolver_url);
      }
      //we use our local institution name b/c editors can make local changes
      self::set_institution_name($row->institution_name);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  
  /*
  * Simple Stats Table - No UI currently available for this
  */
  public function set_stats($nid, $user_type, $rid) {
    db_query("INSERT INTO {resolvers_stats} (nid, user_type, rid, unix_timestamp) VALUES (%d, %d, %d, %d)",
      $nid, $user_type, $rid, time()
    );
  }
  
  /*
  * deletes a user's resolver preference setting
  */
  public function resolver_pref_unset($uid) {
    $uid = (int) $uid;
    if ($uid) {
      db_query('DELETE FROM {resolvers_uid_rid} WHERE uid=%d', $uid);
    }
  }

}