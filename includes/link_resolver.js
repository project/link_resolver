$(document).ready(function(){
	if ($('#edit-override-resolver-url').is(':checked') == false) {
		$('#edit-resolver-url').attr("disabled", true);
		$('#edit-resolver-url').css('background','#ccc');
	}
	
	$("#edit-override-resolver-url").click( 
		function () { 
			if ($('#edit-override-resolver-url').is(':checked') == false) {
				$('#edit-resolver-url').attr("disabled", true);
				$('#edit-resolver-url').css('background','#ccc');
			}
			else {
				$('#edit-resolver-url').removeAttr("disabled"); 
				$('#edit-resolver-url').css('background','none');			
			}
		});
});