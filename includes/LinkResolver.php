<?php
/**
 * @file
 * LinkResolver class file
 *
 */
 
 /**
  * LinkResolver
  *
  * Provides class data and accessors for Link Resolver Classes
  * 
  * @package LinkResolver
  * @author Chad Fennell
  **/

class LinkResolver {
  private $_registry_id; //WorldCat Institution Registry ID
  private $_rid; //local resolver ID value
  private $_resolver_url;
  private $_institution_name;
  private $_uid;
  private $_prid;
  
  /*
  * getter for resolver_url
  */
  public function get_resolver_url() {
    return $this->_resolver_url;
  }

  /*
  * getter for institution_name
  */
  public function get_institution_name() {
    return $this->_institution_name;
  }
  
  /*
  * getter for rid
  */
  public function get_rid() {
    return $this->_rid;
  }
  
  /*
  * getter for uid
  */
  public function get_uid() {
    return $this->_uid;
  }  
  
  /*
  * getter for registry_id
  */
  public function get_registry_id() {
    return $this->_registry_id;
  }  
  
  public function get_resolvers() {
    $results = db_query('SELECT * FROM {resolvers} ORDER BY institution_name');
    $resolvers = array();    
    while ($row = db_fetch_object($results)) {
      $resolvers[$row->rid] = $row->institution_name;
    }
    return $resolvers;
  }
  
  public function get_resolvers_without_parents() {
    $results = db_query('SELECT * FROM {resolvers} WHERE prid < 1 ORDER BY institution_name');
    $resolvers = array();    
    while ($row = db_fetch_object($results)) {
      $resolvers[$row->rid] = $row->institution_name;
    }
    return $resolvers;
  }
  
  public function get_prid() {
    return $this->prid;
  }
  
  /*
   * setter for rid
   */
   public function set_rid($rid) {
     $this->_rid = (int) $rid;
   }

   /*
   * setter for user
   * need to be able to set user manually to be testable
   */
   public function set_uid($uid) {
     $this->_uid = (int) $uid;
   }  

   /*
   * setter for resolver_url
   */
   public function set_resolver_url($resolver_url) {
     $this->_resolver_url= check_plain($resolver_url);
   }

   /*
   * setter for institution_name
   */
   public function set_institution_name($institution_name) {
     $this->_institution_name = check_plain($institution_name);
   }

   /*
   * setter for registry_id
   */
   public function set_registry_id($registry_id) {
     $this->_registry_id = (int) $registry_id;
   }

   /*
   * setter for prid
   */
   public function set_prid($prid) {
     $this->prid = (int) $prid;
   }
   
   public function get_resolver_for_uid($uid) {
     $result =  db_query('SELECT * FROM {resolvers_uid_rid} AS rur, {resolvers} AS r WHERE r.rid=rur.rid AND rur.uid=%d', $uid);
     return db_fetch_object($result);
   }
   
   /*
   * Check and see if we have a prid set
   */
   public function get_prid_for_rid() {
     $result = db_query('SELECT prid FROM {resolvers} WHERE rid=%d', self::get_rid());
     $row = db_fetch_object($result);
     return ($row->prid) ? $row->prid : NULL;
   }   
   
   /*
   * @return object resolver
   */
   public function lookup_by_rid() {
     if (self::get_rid()) {
       $result = db_query('SELECT * FROM {resolvers} WHERE rid=%d', self::get_rid());
       return db_fetch_object($result);      
     }
   }

   /*
   * @return object resolver
   */
   public function local_lookup_by_registry_id($registry_id) {
     if ($registry_id) {
       $result = db_query('SELECT * FROM {resolvers} WHERE registry_id=%d', $registry_id);
       return db_fetch_object($result);      
     }
   }
   
   /*
   * @return object resolver
   */
   public function check_duplicate($institution_name, $registry_id = NULL) {
     if ($registry_id) {
       $result = db_query("SELECT * FROM {resolvers} WHERE institution_name='%s' 
         OR registry_id=%d", $institution_name, $registry_id);
       return db_fetch_object($result);      
     }
   }   
   
   /**
    * Helper function to append the correct suffix to resolver urls
    */
   protected function fix_resolver_urls($url) {
    if (!preg_match('|^http(s)?://|', $url)) {
      $prefix = 'http://';
    }

    return $prefix . $url . $suffix;
   }
   
   public function gateway_lookup_by_registry_id($registry_id) {
     $registry_id = (int) $registry_id;
     $url = sprintf('http://www.worldcat.org/webservices/registry/enhancedContent/Institutions/%d', $registry_id);
     //welcome to the arrow anti-pattern
     if ($xml = simplexml_load_file($url)) {
       foreach ($xml->openURL->records as $record) {
         foreach ($record->children() as $institution) {
           foreach ($institution->children() as $resolver) {
             if ($resolver->baseURL) {
               $registry_id      = (string) $institution->InstitutionID;                      
               $inst             = (string) $institution ->institutionName;
               $url              = (string) self::fix_resolver_urls($resolver->baseURL);
               $resolvers_all[$registry_id]  = array('institution_name' => $inst, 'resolver_url' => $url);
               if (!stripos($resolver->baseURL, 'illiad')) {
                 $resolvers_preferred[$registry_id] = array('institution_name' => $inst, 'resolver_url' => $url);
               }
             }
           }
         }
       }
     }

     if (count($resolvers_preferred) >= 1) {
       rsort($resolvers_preferred);
       $resolver = array_pop($resolvers_preferred);
     }
     else {
       rsort($resolvers_all);
       $resolver = array_pop($resolvers_all);
     }

     return $resolver;
   }
}